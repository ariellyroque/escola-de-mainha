let disciplinas = [];
class Disciplina {
  constructor(Matéria, Professor, Aulas) {
    this.matéria = Matéria;
    this.professor = Professor;
    this.aulas = Aulas;
  }
}

function cadastrarDisciplinas() {
  let matéria = byId("nomeDisciplina").value;
  let professor = byId("nomeProfessor").value;
  let aulas = byId("quantAulas").value;
  let disciplina = new Disciplina(matéria,professor, aulas);
  disciplinas.push(disciplina);
  apresentarDisciplinas();
}

function apresentarDisciplinas() {
  let tabela = byId("tabela");
  let listagem  = "<ol>"
  for(let disciplina of disciplinas) {
    listagem += `<li>${disciplina.matéria}, ${disciplina.professor}, ${disciplina.aulas}</li>`;
  }
  listagem += "</ol>" ;
  tabela.innerHTML = listagem;
}
function byId(id) {
  return document.getElementById(id);
}